from __future__ import print_function
import datetime
import json


def about_handler(event, context):
    """
    Returns some very simple data about the api
    :param event:
    :param context:
    :return:
    """
    print("Handling about request %s" % datetime.datetime.utcnow())
    operation = event['httpMethod']
    body = {
        "message": "This is the about endpoint for %s (event=%s)" % (context.client_context,
                                                                     type(event)),
        "version": context.function_version
    }
    return {
        'statusCode': 200,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
        }
    }